from os.path import join
FASTQ_DIR = "data/"
sample = config['samples']

"""
This rule maps the fastq files on the genome with the burrow wheeler alignment (bwa) in
combination with samtools
"""
rule bwa_map:
    input:
        join(FASTQ_DIR, "genome.fa"),
        join(FASTQ_DIR, "samples/{sample}.fastq")
    output:
        "mapped_reads/{sample}.bam"
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"


"""
uses the samtools application to sort the reads
"""
rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"


"""
Uses samtools to index the bam files and create a bam.bai file.
"""
rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input}"


"""
Variant calling using bcftools
"""
rule bcftools_call:
    input:
        fa=join(FASTQ_DIR, "genome.fa"),
        bam=expand("sorted_reads/{sample}.bam", sample=config['samples']),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=config['samples'])
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

"""
Creates a small report containing the number of variants and a short description of the workflow
"""
rule report:
    input:
        "calls/all.vcf"
    output:
        "out.html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])

